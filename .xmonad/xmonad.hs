import XMonad
import XMonad.Util.EZConfig
import XMonad.Util.Run
import XMonad.Actions.GridSelect
import XMonad.Actions.UpdatePointer
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.SetWMName
--import XMonad.Hooks.ICCMFocus
--  (isFullscreen, isDialog, doFullFloat, doCenterFloat)
import XMonad.Layout.NoBorders
import XMonad.Layout.PerWorkspace
import XMonad.Layout.Gaps
import XMonad.Layout.ResizableTile
import XMonad.Layout.Spacing
import XMonad.Layout.SimpleFloat

import Data.Monoid
import System.Exit
import System.IO

import qualified XMonad.StackSet as W
import qualified Data.Map        as M

myNormalBorderColor     = "#002b36"
--myFocusedBorderColor    = "#799fbf"
myFocusedBorderColor    = "#005f5f"
colorXmobarActive       = "#ffffff"
colorXmobarInactive     = "#919191"
colorXmobarTitle        = "#ffffff"

onSpace :: Int -> [Char]
onSpace x = myWorkspaces !! (x-1)

main :: IO()
main = xmonad =<<
  statusBar "xmobar" myPP toggleStrutsKey myConfig
  --statusBar "xmobar -x 1 /home/lpvb/.xmobarrc2" myPP toggleStrutsKey =<< 
  --statusBar "xmobar -x 2 /home/lpvb/.xmobarrc3" myPP toggleStrutsKey myConfig
myConfig = defaultConfig
  { terminal           = myTerminal
  , focusFollowsMouse  = myFocusFollowsMouse
  , borderWidth        = myBorderWidth
  , modMask            = myModMask
  , workspaces         = myWorkspaces
  , normalBorderColor  = myNormalBorderColor
  , focusedBorderColor = myFocusedBorderColor
  , keys               = myKeys
  , mouseBindings      = myMouseBindings
  , layoutHook         = myLayout
  , manageHook         = myManageHook
  , handleEventHook    = myEventHook
  , startupHook        = myStartupHook
  , logHook            = updatePointer (Relative 0.9 0.9)
  }

myTerminal          = "urxvtc"
myFocusFollowsMouse = True
myBorderWidth       = 1
myModMask           = mod4Mask --mod1Mask
myWorkspaces        = ["1","2","3","4","5","6","7","8","9"]

toggleStrutsKey XConfig {XMonad.modMask = modMask} = (modMask, xK_b)

myPP = xmobarPP
  { ppCurrent = xmobarColor colorXmobarActive ""
  , ppHidden = xmobarColor colorXmobarInactive ""
  , ppTitle = xmobarColor colorXmobarTitle "" . shorten 50
  , ppSep = " .: "
  , ppLayout = const ""
  }

myKeys = \conf -> mkKeymap conf $
  -- Terminal launch
  [ ("M-<Return>", spawn $ terminal conf)

  -- Program exit
  , ("M1-<F4>", kill)

  -- Layout switching
  , ("M-<Space>", sendMessage NextLayout)
  , ("M-S-<Space>", setLayout $ XMonad.layoutHook conf)

  -- Autocomplete program launcher
  , ("M-p", spawn "dmenu_run")

  -- Traditional program menu
  , ("M-S-p", spawn "xmenud")

  -- Window focusing & moving
  , ("M-n", refresh)
  , ("M-<Tab>", windows W.focusDown)
  , ("M-j", windows W.focusDown)
  , ("M-S-j", windows W.swapDown)
  , ("M-k", windows W.focusUp)
  , ("M-S-k", windows W.swapUp)
  , ("M-m", windows W.focusMaster)
  , ("M-S-m", windows W.swapMaster)
  , ("M-t", withFocused $ windows . W.sink)

  -- Column window count inc/dec
  , ("M-S-=", sendMessage (IncMasterN 1))
  , ("M-S--", sendMessage (IncMasterN (-1)))

  --, ("M-b", sendMessage ToggleStruts)
  , ("M-S-q", io (exitWith ExitSuccess))
  , ("M-q", spawn "xmonad --recompile; xmonad --restart")

  -- WSAD window resizing
  , ("M-s", sendMessage MirrorShrink) --Horizontal resize
  , ("M-w", sendMessage MirrorExpand)
  , ("M-a", sendMessage Shrink) --Vertical resize
  , ("M-d", sendMessage Expand)

  , ("M-g", goToSelected defaultGSConfig) --Grid Select
  , ("<Print>", spawn "scrot -e 'mv $f ~/screenshots'")
    
    -- Laptop/media keyboard keys
  , ("<XF86AudioMute>", spawn "amixer set Master toggle")
  , ("<XF86AudioRaiseVolume>", spawn "amixer set Master playback 3+")
  , ("<XF86AudioLowerVolume>", spawn "amixer set Master playback 3-")
  , ("<XF86AudioPlay>", spawn "ncmpcpp play")
  , ("<XF86AudioStop>", spawn "ncmpcpp stop")
  , ("<XF86AudioNext>", spawn "ncmpcpp next")
  , ("<XF86AudioPrev>", spawn "ncmpcpp prev")
  , ("<XF86AudioPause>", spawn "ncmpcpp toggle")

    -- Media backup control keys
  , ("M-.", spawn "ncmpcpp next")
  , ("M-,", spawn "ncmpcpp prev")
  , ("M-/", spawn "ncmpcpp toggle")
  , ("M-S-.", spawn "amixer set Master playback 3+") 
  , ("M-S-,", spawn "amixer set Master playback 3-")
  ]

  ++

    --
    -- mod-[1..9], Switch to workspace N
    -- mod-shift-[1..9], Move client to workspace N
    --
  [ ("M-" ++ m ++ k, windows $ f i)
    | (i, k) <- zip (XMonad.workspaces conf) $ map show [1 .. 9]
    , (f, m) <- [(W.greedyView, ""), (W.shift, "S-")]]

  {-
     [((m .|. modm, k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf) ["1" .. "9"]
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]
    -}

  -- ++
    --
    -- mod-{w,e,r}, Switch to physical/Xinerama screens 1, 2, or 3
    -- mod-shift-{w,e,r}, Move client to screen 1, 2, or 3
    --
    --[((m .|. modm, key), screenWorkspace sc >>= flip whenJust (windows . f))
    --    | (key, sc) <- zip [xK_w, xK_e, xK_r] [0..]
    --    , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]
  ++
  [ ("M-" ++ m ++ key, screenWorkspace sc >>= flip whenJust (windows . f))
      | (key, sc) <- zip ["<Left>", "<Right>", "<Up>"] [0..]
      , (f, m) <- [(W.view, ""), (W.shift, "S-")]]


------------------------------------------------------------------------
-- Mouse bindings: default actions bound to mouse events
--
myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $
  [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w))
  , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))
  , ((modm, button3), (\w -> focus w >> mouseResizeWindow w 
    >> windows W.shiftMaster))
  ]

myLayout =
  --noBorders $ avoidStruts $ onWorkspace (onSpace 9) (noBorders Full) $
  lessBorders Screen $ avoidStruts $ onWorkspace (onSpace 9) simpleFloat $
  resizable ||| Mirror tiled ||| Full
  where
    tiled   = Tall nmaster delta ratio
    resizable = ResizableTall nmaster delta ratio []
    nmaster = 1
    ratio   = 1/2
    delta   = 3/100

myManageHook =
  composeAll
    [ className =? "MPlayer" --> doFloat
    , className =? "com-intellij-rt-execution-application-AppMain" --> doFloat
    , className =? "Pavucontrol" --> doFloat
    , className =? "Skype" --> doFloat
    , className =? "Vncviewer" --> doFloat
    , className =? "Vlc" --> doFloat
    , className =? "Gimp-2.8" --> doFloat
    , className =? "Qsynergy" --> doFloat
    , title =? "Minecraft" --> doFloat
    , title =? "Minecraft Launcher" --> doFloat
    , resource  =? "desktop_window" --> doIgnore
    , className =? "mplayer" --> doFloat
    , title =? "opengl" --> doFloat
    , isFullscreen --> doFullFloat
    , isDialog --> doCenterFloat
    ]
  where wmName = stringProperty "WM_NAME"

myEventHook = fullscreenEventHook
 
myStartupHook = setWMName "LG3D"
