# Dotfiles

These dotfiles provide a minimum setup for my working environment. It provides
a solarized dark color scheme, battery scripts, xmonad configuration, etc.

## Installation

Clone into your home directory. Then manually install Vundle for vim, unclutter,
alsa utils, pulseaudio, xorg-xsetroot, xorg-xset, rxvt-unicode-patched, trayer,
xmonad, xmonad-contrib, xbindkeys, terminus font, and oh-my-zsh.

I use lxappearance to set my gtk theme to Flatts-Turquoise. Move 
.hidden/scripts/qt5style.sh to /etc/profile.d/ to set Qt theme to follow gtk and
qtconfig for qt4 applications.


For git ignore use command `git config --global core.excludesfile ~/.gitignore_global`

## License

Copyright 2013-2014 (C) louispvb <louispvb@gmail.com> MIT license
