#!/bin/bash

remove_device () {
BDF=$1
# Unbind a PCI function from its driver as necessary
[ ! -e /sys/bus/pci/devices/$BDF/driver/unbind ] || \
echo -n $BDF > /sys/bus/pci/devices/$BDF/driver/unbind
# Add a new slot to the PCI Backend's list
echo -n $BDF > /sys/bus/pci/drivers/pciback/new_slot
# Now that the backend is watching for the slot, bind to it
echo -n $BDF > /sys/bus/pci/drivers/pciback/bind
}

#usb controller
remove_device "0000:00:1a.0"
remove_device "0000:00:1d.0"
#HD 7970
remove_device "0000:01:00.0"
remove_device "0000:01:00.1"
#audio
remove_device "0000:01:1b.0"

xl create /etc/xen/win7a.sxp
