#!/bin/bash

battery=$(acpi -b)

percent=$(echo $battery | cut -d ' ' -f 4)
remainingtime=$(echo $battery | cut -d ' ' -f 5 | cut -d ':' -f -2)

echo "$percent $remainingtime"
