set nocompatible
"we-------------------------------------------------------------------------------
set rtp+=~/.vim/bundle/Vundle.vim/
call vundle#begin()

" Vundle Bundle 
Bundle 'gmarik/Vundle.vim'

Bundle 'altercation/vim-colors-solarized'
Bundle 'guns/xterm-color-table.vim'
"Allows 'juggling' of buffers by pressing ',l' and fuzzy finding by ',t'
Bundle 'LustyJuggler'
Bundle 'wincent/Command-T'
Bundle 'scrooloose/nerdtree'
"Vim Powerline (more useful status line)
Bundle 'Lokaltog/vim-powerline'
"Scala syntax and indent
Bundle 'derekwyatt/vim-scala'

"Clojure syntax and indent
Bundle 'vim-scripts/VimClojure'
"Allows tabularization/alignment of code
Bundle 'godlygeek/tabular'

"Use Tab for all completion
Bundle 'SuperTab'

"gist pasting
Bundle 'mattn/gist-vim'

"Sane indentation of haskell code
Bundle 'indenthaskell.vim'
"Lots of haskell convenience scripts
Bundle 'dag/vim2hs'
"Omni-completion of many source files, used by neco-ghc
Bundle 'Shougo/neocomplcache'
"Haskell full autocompletion using ghc-mod
Bundle 'ujihisa/neco-ghc'
"C++ full autocompletion using clang
Bundle 'Rip-Rip/clang_complete'

"Rust for Retep998
Bundle 'wting/rust.vim'
call vundle#end()
"-------------------------------------------------------------------------------

"Color Scheme
set background=dark
colorscheme solarized

let vimclojure#ParenRainbow=1
let vimclojure#HighlightBuiltins=1


"Set NERDTree default width
let g:NERDTreeWinSize = 18
"GVim Options :help 'guioptions'
set guioptions=aegimrLt
set guifont=Envy\ Code\ R\ 10
"set gfn=Source Code Pro Ultra-Light 10 
"Clang automplete fix
"let g:clang_user_options='|| exit 0'
"let g:clang_complete_copen = 1
"Don't consider C++ lambdas as errors
let c_no_curly_error = 1

" Key mapping
let mapleader = ","

au VimEnter * unmap <Leader>lj
"nmap <Leader>t :LustyFilesystemExplorerFromHere<CR>
nmap <Leader>l :LustyJuggler<CR>


let g:haskell_indent_if = 2
let g:haskell_indent_case = 2
"let g:haskell_conceal_wide = 1
"let g:haskell_conceal = 0
let g:hpaste_author = 'lpvb'
let g:haskell_tabular = 1

let g:clang_use_library = 1
let g:clang_snippets = 1
let g:clang_snippets_engine = 'clang_complete'

let g:neocomplcache_enable_at_startup = 1

let g:haskell_conceal_enumerations = 0



filetype plugin indent on
syntax on
autocmd FileType python setlocal expandtab shiftwidth=4 softtabstop=4
au BufNewFile,BufRead *.glsl set filetype=glsl
au BufRead,BufNewFile *.scala set filetype=scala



"Horizontal Ruler
function! Repeat(count)
  "let times = input("Count: ")
  "let char = input("Char: ")
  exe ":normal a" . repeat("-", a:count)
endfunction

autocmd FileType cpp imap <buffer> <C-u> //<C-o>:call Repeat(78)<cr><cr><C-H><C-H>
autocmd FileType haskell imap <buffer> <C-u> <C-o>:call Repeat(80)<cr><cr><C-H><C-H><C-H>
"Paste header license
command! Head :0r ~/.vim/HEADER

"Buffer Navigation Keys
nmap <C-c> :bd<CR>
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l

set completeopt=menuone,longest

" Always show status line
set laststatus=2

"Disable stupid folding.
set nofoldenable

"Visualize Whitespace
"set list
"set listchars=tab:>.,trail:.,extends:#,nbsp:.
" Hide buffers when there are unsaved changes instead of demanding save
set hidden

" Indenting
set autoindent
set expandtab
set tabstop=2
set shiftwidth=2
set softtabstop=2

" Line number and 80 char column
set number
set colorcolumn=81

" Show autocompletion menu
set wildmenu

" Show current pos
set ruler

" Highlight search terms
set hlsearch

" Show matching brackets
set showmatch

" Wrap search
set wrapscan
set incsearch

" Ignore search case if only lower case
set smartcase

" No backup
set nobackup
set noswapfile
set nowritebackup
" Set textwidth
" set textwidth=79

" Mouse support
set mouse=a

" Pase mode
set pastetoggle=<F2>

" Set backspace
set backspace=indent,eol,start

" Wrap lines
" set wrap

" Shows what you are typing
set showcmd
set foldmethod=marker

" Autowrite buffer on switch
set autowrite

" set background=dark
" Set 256 color support
set t_Co=256

"Set terminal encoding
set termencoding=utf-8
set encoding=utf-8

"Vim optimizations
set lazyredraw
set ttyfast
set history=100
